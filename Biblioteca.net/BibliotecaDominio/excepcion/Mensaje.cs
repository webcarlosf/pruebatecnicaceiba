﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BibliotecaDominio.excepcion
{
    public class Mensaje
    {
        public Mensaje() { }

        public int Codigo
        {
            get;
            set;
        }

        public string Texto
        {
            get;
            set;
        }
    }
}
