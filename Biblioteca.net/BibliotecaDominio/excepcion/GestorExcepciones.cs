﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BibliotecaDominio.excepcion
{
    public class GestorExcepciones : Exception
    {
        public GestorExcepciones(string message) : base(message)
        {
        }
    }
}
