﻿using BibliotecaDominio.excepcion;
using BibliotecaDominio.IRepositorio;
using System;
using System.Collections.Generic;
using System.Text;

namespace BibliotecaDominio
{
    public class Bibliotecario
    {
        public const string EL_LIBRO_NO_SE_ENCUENTRA_DISPONIBLE = "El libro no se encuentra disponible";
        public const string EL_ISBN_ES_PALINDORMO = "Los libros palíndromos solo se pueden utilizar en la biblioteca";
        
        private  IRepositorioLibro libroRepositorio;
        private  IRepositorioPrestamo prestamoRepositorio;
        Mensaje mensaje = null;

        public Bibliotecario(IRepositorioLibro libroRepositorio, IRepositorioPrestamo prestamoRepositorio)
        {
            this.libroRepositorio = libroRepositorio;
            this.prestamoRepositorio = prestamoRepositorio;
        }

        public void Prestar(string isbn, string nombreUsuario)
        {
            if (!EsPrestado(isbn))
            {
                if (esPalindromo(isbn))
                {
                    mensaje = new Mensaje()
                    {
                        Texto = EL_ISBN_ES_PALINDORMO
                    };
                    throw new GestorExcepciones(mensaje.Texto);
                }
                else
                {
                    Libro libro = this.libroRepositorio.ObtenerPorIsbn(isbn);
                    Prestamo prestamo = new Prestamo(DateTime.Now, libro, FechaMaximaEntrega(isbn), nombreUsuario);
                    this.prestamoRepositorio.Agregar(prestamo);
                }
            }
            else
            {
                mensaje = new Mensaje()
                {
                    Texto = EL_LIBRO_NO_SE_ENCUENTRA_DISPONIBLE
                };
                throw new GestorExcepciones(mensaje.Texto);
            }
            
        }

        public bool EsPrestado(string isbn)
        {
            Libro libroPrestado = prestamoRepositorio.ObtenerLibroPrestadoPorIsbn(isbn);
            return libroPrestado == null ? false: true;
        }
        // Metodo que dice si un ISBN es palindromo
        public Boolean esPalindromo(string isbn)
        {
            string palabraInvertida = "";
            int i = 0;
            int longitud = isbn.Length - 1;
            do
            {
                palabraInvertida = palabraInvertida + isbn.Substring(longitud, 1);
                i++;
                longitud--;
            } while (i < isbn.Length);

            if (isbn.Equals(palabraInvertida))
            {
                return true;
            }
            return false;

        }

        // Metodo para calcular la Fecha maxima de entrega de un libro
        public DateTime? FechaMaximaEntrega(string isbn)
        {
            if (longuitudisbn(isbn))
            {
                DateTime FechaEntrega = DateTime.Now.AddDays(15);
                DateTime FechaCalcular = DateTime.Now;
                int contadorDomingos = 0;
                int diasSumar = 1;
                do
                {
                    if (FechaCalcular.DayOfWeek == DayOfWeek.Sunday)
                    {
                        contadorDomingos++;
                    }
                    FechaCalcular = DateTime.Now.AddDays(diasSumar);
                    diasSumar++;
                } while (FechaCalcular <= FechaEntrega);

                FechaEntrega = DateTime.Now.AddDays(15 + contadorDomingos);

                if (FechaEntrega.DayOfWeek == DayOfWeek.Sunday)
                {
                    FechaEntrega = DateTime.Now.AddDays(15 + contadorDomingos + 1);
                }

                return FechaEntrega;
            }
            else
            {
                return null;
            }
        }

        // Metodo para calcula si los digisto en un ISBN suman mas de 30 
        private bool longuitudisbn(string isbn)
        {
            char[] digitos = isbn.ToCharArray();
            int sumaDigitos = 0;
            for (int i = 0; i < isbn.Length; i++)
            {
                if (Char.IsNumber(digitos[i]))
                {
                    sumaDigitos = sumaDigitos + int.Parse(digitos[i].ToString());
                }
            }
            return sumaDigitos >= 30;
        }

    }
}
