﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BibliotecaDominio;
using BibliotecaRepositorio.Contexto;
using BibliotecaRepositorio.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.VistaModelo;

namespace WebApp.Controllers
{
    public class PrestamoController : Controller
    {
        private BibliotecaContexto contexto;
        private RepositorioLibroEF repositorioLibro;
        private RepositorioPrestamoEF repositorioPrestamo;

        public PrestamoController()
        {
            var optionsBuilder = new DbContextOptionsBuilder<BibliotecaContexto>();
            contexto = new BibliotecaContexto(optionsBuilder.Options);
            repositorioLibro = new RepositorioLibroEF(contexto);
            repositorioPrestamo = new RepositorioPrestamoEF(contexto, repositorioLibro);
        }

        public IActionResult Index()
        {
            

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ModeloVistaPrestamo prestamo)
        {
            if (ModelState.IsValid)
            {
                Libro libro = new Libro(prestamo.Isbn, "TITULOLIBRO", 2019) { };
                repositorioLibro.Agregar(libro);
                Bibliotecario bibliotecario = new Bibliotecario(repositorioLibro, repositorioPrestamo);

                // Act
                try
                {
                    bibliotecario.Prestar(libro.Isbn, prestamo.NombreUsuario);

                    ViewBag.ModeloVistaPrestamo = repositorioPrestamo.ObtenerLibroPrestadoPorIsbn(libro.Isbn);
                }
                catch (Exception ex)
                {
                    ViewBag.MensageError = ex.Message;
                }

            }

            return View();
        }
    }
}