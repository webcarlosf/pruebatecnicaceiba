﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.VistaModelo
{
    public class ModeloVistaPrestamo
    {
        public string Isbn { get; set; }

        public string NombreUsuario { get; set; }
    }
}
